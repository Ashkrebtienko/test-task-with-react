import React from 'react';

class InputName extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      input: ''
    };
  }

  addTask = () => {
    const { input } = this.state;
    if (input) {
      this.props.addTask(input);
      this.setState({ input: '' });
    }
  };

  handleEnter = event => {
    if (event.key === 'Enter') this.addTask();
  };

  inputChange = event => {
    this.setState({ input: event.target.value });
  };

  render() {
    const { input } = this.state;

    return (
      <div className="task-input">
        <label>
          Name:
          <input
            onKeyPress={this.handleEnter}
            onChange={this.inputChange}
            value={input}
          ></input>
        </label>
        {/* /* <label>
          Text:
          <input
            onKeyPress={this.handleEnter}
            onChange={this.inputChange}
            value={input}
          ></input>
        </label> */ }
      </div>
    );
  }
}

export default InputName;
