import React from 'react';
import Task from './components/Task';
import FormContainer from './containers/FormContainer';

class App extends React.Component {
  constructor() {
    super();

    this.state = { 
      tasks: [
        {id: 0, title: "Create to-do reac-app", done: false, name: "Anton", text: "First comment"},
        {id: 1, title: "Antorher todo", done: true, name: "John", text: "Second comment" },
        {id: 2, title: "One more todo", done: false, name: "Anton", text: "Third comment"}
      ]
    }
  }

  addComment = (e) => { 
    this.setState(state => {
      let {tasks} = state;
      tasks.push({
        id: tasks.length !== 0 ? e.length : 0,
        name: FormContainer.state.name,
        text: FormContainer.state.text,
        done: false,
      })
      return tasks; 
    })

  }


  doneTask = id => {
    const index = this.state.tasks.map(task => task.id).indexOf(id);
    this.setState(state => {
      let {tasks} = state;
      tasks[index].done = true;
      return tasks;
    });
  };

  deleteTask = id => {
    const index = this.state.tasks.map(task => task.id).indexOf(id);
    this.setState(state => {
      let {tasks} = this.state;
      delete tasks[index];
      return tasks; 
    })
  }
  render() {
    const { tasks } = this.state;
    const activeTasks = tasks.filter(task => !task.done);
    const doneTasks = tasks.filter(task => task.done);

    return <div className="App">
      <h1 className="top">List of comments:</h1>
      {[...activeTasks, ...doneTasks].map(task => (
        <Task 
        doneTask = {() => this.doneTask(task.id)}
        deleteTask = {() => this.deleteTask(task.id)}
        task={task}
        key={task.id}
        ></Task>
      ))}
        {/* <InputName addTask={this.addComment}></InputName>
        <InputText addTask={this.addComment}></InputText> */}
        <FormContainer></FormContainer>
    </div>
  }

}

export default App;
