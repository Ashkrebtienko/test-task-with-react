import React, {Component} from 'react';  

/* Import Components */

import Input from '../components/Input';  
import TextArea from '../components/TextArea';  
import Button from '../components/Button'

class FormContainer extends Component {  
  constructor(props) {
    super(props);

    this.state = {
      newComment: {
        id: '',
        name: '',
        text: '',
      },

    }
    this.handleTextArea = this.handleTextArea.bind(this);
    this.handleText = this.handleText.bind(this);
    this.handleFullName = this.handleFullName.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
  }

  /* This lifecycle hook gets executed when the component mounts */
  
  handleFullName(e) {
   let value = e.target.value;
   this.setState( prevState => ({ newComment : 
        {...prevState.newComment, name: value
        }
      }), () => console.log(this.state.newComment))
  }

  handleText(e) {
       let value = e.target.value;
   this.setState( prevState => ({ newComment : 
        {...prevState.newComment, text: value
        }
      }), () => console.log(this.state.newComment))
  }


  handleTextArea(e) {
    console.log("Inside handleTextArea");
    let value = e.target.value;
    this.setState(prevState => ({
      newComment: {
        ...prevState.newComment, text: value
      }
      }), ()=>console.log(this.state.newComment))
  }


  handleFormSubmit(e) {
    e.preventDefault();
    let userData = this.state.newComment;

    fetch('https://jordan.ashton.fashion/api/goods/30/comments',{
        method: "POST",
        body: JSON.stringify(userData),
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
      }).then(response => {
        response.json().then(data =>{
          console.log("Successful" + data);
        })
    })
    
  }   


  handleClearForm(e) {
  
      e.preventDefault();
      this.setState({ 
        newComment: {
          name: '',
          text: ''
        },
      })
  }

  render() {
    return (
    
        <form className="container-fluid" onSubmit={this.handleFormSubmit}>
       
            <Input inputType={'text'}
                   title= {'Name:'} 
                   name= {'name'}
                   value={this.state.newComment.name} 
                   placeholder = {'Enter your name'}
                   handleChange = {this.handleFullName}
                   
                   /> {/* Name of the user */}
        
         
          <TextArea
            title={'Your comment:'}
            rows={10}
            value={this.state.newComment.text}
            name={'currentPetInfo'}
            handleChange={this.handleTextArea}
            placeholder={'Write your comment here'} />{/* About you */}

            <Button 
              addTask={this.addComment}
              action = {this.handleFormSubmit}
              type = {'primary'} 
              title = {'Submit'} 
            style={buttonStyle}
          /> { /*Submit */ }
            <Button 
            action = {this.handleClearForm}
            type = {'secondary'}
            title = {'Clear'}
            style={buttonStyle}
          /> {/* Clear the form */}
        </form>
  
    );
  }
}

const buttonStyle = {
  margin : '10px 10px 10px 10px'
}

export default FormContainer;